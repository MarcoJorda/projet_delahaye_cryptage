"""
Module de codage contenant les fonctions permettant de crypter/ décrypter.
Celles-ci sont appellées grâce au callback_xxxxxx dans interface_codage_cesar.py
"""
import string


# Initialisation du décalage


def crypter_non_circulaire(chaine,decalage):
    chaine_cryptee=""
    for caractere in chaine:
        if caractere ==" ": #puisque l'on ne code pas les espaces, on vérifie que le caractère n'en n'est pas un
            chaine_cryptee= chaine_cryptee+" "
        else:
            chaine_cryptee = chaine_cryptee + chr(ord(caractere)+decalage)
    return chaine_cryptee

def decrypter_non_circulaire(chaine,decalage):
    chaine_decryptee=""
    for caractere in chaine:
        if caractere == " ": #puisque l'on ne code pas les espaces, on vérifie que le caractère n'en n'est pas un
            chaine_decryptee = chaine_decryptee + " "
        else : 
            chaine_decryptee = chaine_decryptee + chr(ord(caractere)-decalage)
    return chaine_decryptee

"""
A noter qu'il est possible de regrouper crypter_non_circulaire et décrypter non circulaire en une seule fonction.
Il suffirai simplement d'envoyer +decalage pour crypter et -décalage pour décrypter.
"""
def crypter_circulaire(chaine,decalage):
    
    M_alphabet = string.ascii_uppercase
    m_alphabet = string.ascii_lowercase

    a_crypte = cryptage_alphabet(m_alphabet,decalage)   
    A_crypte = cryptage_alphabet(M_alphabet,decalage)

    chaine_cryptee = conversion_chaine(chaine,a_crypte, A_crypte, m_alphabet, M_alphabet)

    return chaine_cryptee


def decrypter_circulaire(chaine,decalage):

    M_alphabet = string.ascii_uppercase
    m_alphabet = string.ascii_lowercase

    a_crypte = cryptage_alphabet(m_alphabet,decalage)  
    A_crypte = cryptage_alphabet(M_alphabet,decalage)

    chaine_decryptee = conversion_chaine(chaine,m_alphabet,M_alphabet, a_crypte, A_crypte)

    return chaine_decryptee


def cryptage_alphabet(alphabet,decalage):

    #decalage = obtenir_decalage()
    alphabet_crypte = alphabet[decalage:]+alphabet[:decalage]

    return alphabet_crypte


"""
INUTILE
def decryptage_alphabet(alphabet):

    alphabet_decrypte = alphabet[:DECALAGE]+alphabet[DECALAGE:]

    return alphabet_decrypte
"""

    
def cryptage_lettre(lettre,alpha_normal, alpha_crypte):

    lettre_cryptee = alpha_crypte[alpha_normal.index(lettre)]

    return lettre_cryptee


def conversion_chaine(chaine,a_crypte, A_crypte,m_alphabet,M_alphabet):

    chaine_cryptee = ""

    for lettre in chaine :
        if lettre in m_alphabet : 

            chaine_cryptee = chaine_cryptee + cryptage_lettre(lettre, m_alphabet, a_crypte)

        elif lettre in M_alphabet:

            chaine_cryptee = chaine_cryptee + cryptage_lettre(lettre, M_alphabet, A_crypte)

        else : 

            chaine_cryptee = chaine_cryptee + lettre
            
    return chaine_cryptee


