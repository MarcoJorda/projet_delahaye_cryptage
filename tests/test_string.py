import unittest
from interface_codage_cesar import *
from codage_cesar.fonctions_codage import *



class StringTest(unittest.TestCase):

    def test_chaine(self):        
        self.assertTrue(check_chaine("Michel Berger"))
        with self.assertRaises(Exception):
            check_chaine(None)


class CodageTest(unittest.TestCase):

    def test_crypter(self):
        #Test du cryptage circulaire
        
        self.assertEqual(crypter_circulaire("bonjour",8),"jwvrwcz")

        #Tests des cryptages non circulaires        
        self.assertEqual(crypter_non_circulaire("BONJOUR",8),"JWVRW]Z")
        self.assertEqual(crypter_non_circulaire("Zèbre:n°2",8),"bðjzmBv¸:")
        self.assertEqual(crypter_non_circulaire(" ",8)," ")
        

    def test_decrypter(self):
        #Test du cryptage circulaire

        self.assertEqual(decrypter_circulaire("jwvrwcz",8),"bonjour")
        #Test des decryptages non circulaires

        
        self.assertEqual(decrypter_non_circulaire("JWVRW]Z",8),"BONJOUR")
        self.assertEqual(decrypter_non_circulaire("AU REVOIR MICHEL",8),"9M J=NGAJ EA;@=D")
        self.assertEqual(decrypter_non_circulaire('ZÈBRE:N°2',8),"RÀ:J=2F¨*")
        self.assertEqual(decrypter_non_circulaire(" ",8)," ")
        