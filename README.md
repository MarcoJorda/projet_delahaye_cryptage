# Projet_Delahaye_Cryptage
# Ce projet a pour but de permettre au client de choisir un décalage et de crypter une chaine de caractères. Le cryptage de la chaine de caractères se fera en fonction du mode que l'utilisateur choisit : Il peut être circulaire (entre a et z inclus), ou bien non circulaire (si le décalage dépasse Z, on continue).
# Il est également possible d'écrire une chaîne de caractères et de la décrypter. 
# Ce programme a été réalisé par Alexis DEMAREST et Marco JORDA. 
# Groupe G1 --- 2ème année de DUT Informatique. 
