"""
Module d'interface utilisant tKinter.
Cette interface sert a obtenir une chaine de caractères 
et la crypter/décrypter.
Les fonctions crypter/décrypter sont dans le module 'codage_cesar.fonctions_codage".
"""


from tkinter import *
from tkinter import messagebox
from codage_cesar.fonctions_codage import *

def check_chaine(chaine):
    """
    Vérification de la présence de chaine à traduire
    """
    if len(chaine) == 0  :
        messagebox.showwarning(title='Erreur !', message="Vous n'avez pas mis de texte à crypter/décypter")
        raise Exception("Il n'y a aucune chaîne à (dé)crypter.")
        return False
    return True

def check_decalage(decalage):
    """
    Vérification de la présence d'un décalage à effectuer
    """

    if decalage is None:
        messagebox.showwarning(title='Erreur !', message="Vous n'avez pas mis de décalage valide pour crypter/décypter")
        raise Exception("Entrez un nombre de décalage pour pouvoir crypter / décrypter")
        return False
    else : 
        if 0<decalage and decalage<27 : 
                return True
        else :
            messagebox.showwarning("Erreur !", "Vous devez entrer une valeur entre 1 et 26 !")  
            return False


def obtenir_texte_saisi():
    """
    Obtenir le texte 
    """
    texte_a_crypter = texte_saisi.get()
    return texte_a_crypter

def obtenir_decalage():
    """
    Obtenir le décalage
    """
    var_decalage = decalage_texte.get()
    try : 
        return int(var_decalage)
    except ValueError : 
        messagebox.showwarning(title='Erreur !', message="Vous n'avez pas mis de décalage pour crypter/décypter")
        raise Exception("Il faut entrer un nombre pour crypter/décrypter à la César !")
        
    
def supprimer_texte():
    """
    Supprimer le texte
    """
    texte_traduit.delete('0', END)


def callback_crypter():
    """
    Callback de crypter
    """
    if check_chaine(obtenir_texte_saisi()) and check_decalage(obtenir_decalage()):
        
        chaine = obtenir_texte_saisi()
        chaine_cryptee = crypter_non_circulaire(chaine,obtenir_decalage())
        supprimer_texte()
        afficher_resultat(chaine_cryptee)

def callback_crypter_circulaire():
    """
    Callback de crypter circulaire
    """
    if check_chaine(obtenir_texte_saisi()) and check_decalage(obtenir_decalage()):
        
        chaine = obtenir_texte_saisi()
        chaine_cryptee = crypter_circulaire(chaine,obtenir_decalage())
        supprimer_texte()
        afficher_resultat(chaine_cryptee)

def callback_decrypter():
    """
    Callback de crypter
    """
    if check_chaine(obtenir_texte_saisi()) and check_decalage(obtenir_decalage()):
        chaine = obtenir_texte_saisi()
        chaine_decryptee = decrypter_non_circulaire(chaine,obtenir_decalage())
        supprimer_texte()
        afficher_resultat(chaine_decryptee)

def callback_decrypter_circulaire():
    """
    Callback de crypter
    """
    if check_chaine(obtenir_texte_saisi()) and check_decalage(obtenir_decalage()):
        chaine = obtenir_texte_saisi()
        chaine_decryptee = decrypter_circulaire(chaine,obtenir_decalage())
        supprimer_texte()
        afficher_resultat(chaine_decryptee)

def afficher_resultat(chaine_postprocess):
    """
    Afficher le résultat crypté/décrypté
    """
    texte_traduit.insert(END,chaine_postprocess)



color_blue = "#2980b9"
color_lightblue = "#3498db"
color_white = "#ecf0f1"
color_black = "#2c3e50"
color_grey = "#7f8c8d"
color_lightgrey = "#95a5a6"
color_red = "#c0392b"
color_lightred = "#e74c3c"

frame = Tk()
frame.title("Le superbe (Dé)Codeur César")
frame.maxsize(1000,1200)
frame.minsize(600,100)


for i in range(10):
    frame.rowconfigure(i, weight = 1)
    
for i in range (5):
    frame.columnconfigure(i,weight = 1)

texte_saisi = Entry(frame,
                bg=color_white,            
                fg=color_black)
texte_saisi.grid(row=1,column = 1, rowspan = 4)
texte_saisi.insert(0, 'Texte ?')

texte_traduit = Entry(frame,
                bg=color_white,
                fg=color_black)
texte_traduit.grid(row=4,rowspan = 3, column = 1)
texte_traduit.insert(0, 'Résultat ?')

decalage_texte = Entry(frame,
                bg=color_white,
                fg=color_black)
decalage_texte.grid(row=0, column = 1, columnspan=1)
decalage_texte.insert(0, 'Décalage ?')

btn_crypter = Button(frame,
                     text='Cryptage non circulaire.',
                     command=callback_crypter,
                     bg=color_blue,
                     activebackground=color_lightblue)
btn_crypter.grid(row=1, column=0)


btn_crypter_circulaire = Button(frame,
                        text='Cryptage circulaire',
                        command=callback_crypter_circulaire,
                        bg=color_blue,
                        activebackground=color_lightblue,
                        fg=color_white,
                        activeforeground=color_black)
btn_crypter_circulaire.grid(row=2, column=0)


btn_decrypter = Button(frame,
                       text='Décryptage non circulaire.',
                       command=callback_decrypter,
                       bg=color_red,
                       activebackground=color_lightred,
                       fg=color_black,
                       activeforeground=color_black)
btn_decrypter.grid(row=1, column=2)


btn_decrypter_circulaire = Button(frame,
                       text='Décryptage circulaire.',
                       command=callback_decrypter_circulaire,
                       bg=color_red,
                       activebackground=color_lightred,
                       fg=color_black,
                       activeforeground=color_black)
btn_decrypter_circulaire.grid(row=2, column=2)


frame.mainloop()
